﻿using AuthenticationPlugin;
using CinemaApi.Data;
using CinemaApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CinemaApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private CinemaDbContext _dbContext;

        //** Below two lines codeis for JWT process as per doc..
        private IConfiguration _configuration;
        private readonly AuthService _auth;

        public UsersController(CinemaDbContext dbContext, IConfiguration configuration)
        {
            //** Below two lines codeis for JWT process as per doc..
            _configuration = configuration;
            _auth = new AuthService(_configuration);

            _dbContext = dbContext;
        }

        [HttpPost]
        public IActionResult Register([FromBody] User user) 
        {
            var userWithSameEmail =_dbContext.Users.Where(u => u.Email == user.Email).SingleOrDefault();

            if(userWithSameEmail != null)
            {
                return BadRequest("Email alrready Exists.");
            }

            var userObj = new User
            {
                Name = user.Name,
                Email = user.Email,
                Password = SecurePasswordHasherHelper.Hash(user.Password),
                Role = "Users"

            };
            _dbContext.Users.Add(userObj);
            _dbContext.SaveChanges();

            return StatusCode(StatusCodes.Status201Created);
        }

        [HttpPost]
        public IActionResult Login([FromBody] User user)
        {
            var userEmail = _dbContext.Users.FirstOrDefault(u => u.Email == user.Email);
            if (userEmail == null)
            {
                return NotFound();
            }
            if (!(SecurePasswordHasherHelper.Verify(user.Password, userEmail.Password)))
            {
                return Unauthorized();
            }

            //** last step of generate the JWT
            var claims = new[]
             {
               new Claim(JwtRegisteredClaimNames.Email, user.Email),
               new Claim(ClaimTypes.Email, user.Email),
               new Claim(ClaimTypes.Role, userEmail.Role)
             };
            //** this below line wll genterate the JWT.
            var token = _auth.GenerateAccessToken(claims);

            //** once generated, we need it in JSON format.. for that below lines..
            return new ObjectResult(new
            {
                access_token = token.AccessToken,
                expires_in = token.ExpiresIn,
                token_type = token.TokenType,
                creation_Time = token.ValidFrom,
                expiration_Time = token.ValidTo,
                //** just add what are the fields to add. here we add the user id.
                user_id = userEmail.Id
            }) ;
        }
    }
}
