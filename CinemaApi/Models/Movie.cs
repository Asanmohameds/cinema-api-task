﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaApi.Models
{
    public class Movie
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="Name must be required, cannot be null or empty")]
        public string Name { get; set; }
        public string Description { get; set; }

        [Required]
        public string Language { get; set; }

        [Required]
        public string Duration { get; set; }

        public DateTime PlayingDate { get; set; }

        public DateTime PlayingTime { get; set; }

        public double TicketPrice { get; set; }

        public double Rating { get; set; }

        public string Genre { get; set; }

        public string TrailorUrl { get; set; }

        //** this part of Movies table to store the Image url, so add migration and update database.
        public string ImageUrl { get; set; }

        // this image property used to store the image but IFormFile data type not support in SQL.
        [NotMapped]
        public IFormFile Image { get; set; }


        //** one many relations with reservation Entities...
        public ICollection<Reservation> Reservations { get; set; }
    }
}
